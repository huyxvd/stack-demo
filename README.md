# Stack (ngăn xếp)
Stack là một cấu trúc dữ liệu tuyến tính tuân theo nguyên tắc Last-In-First-Out (LIFO)
Nghĩa là phần tử cuối cùng được thêm vào stack sẽ là phần tử đầu tiên được loại bỏ khỏi stack.
*Dưới đây là giải thích chi tiết từng bước của pseudocode để triển khai Stack:*
## Khởi tạo một class gồm:

Tạo ra một class với các thuộc tính sau:
- `int[] Items`: Mảng để lưu trữ dữ liệu(số nguyên) của Stack.
- `int MaxStack`: Kích thước tối đa của Stack.
- `int Top`: Vị trí của phần tử cuối trùng trong stack. nếu bằng `-1` nghĩa là stack rỗng giá trị tối đa của `Top = MaxStack - 1`
- ví dụ stack có `MaxStack = 3`(tức là stack chứa được 3 phần tử) thì => giá trị `Top` lớn nhất là `2` => khi này stack đầy

## Constructor stack với yêu cầu

Khởi tạo một Stack mới với các giá trị ban đầu:
- `MaxStack`: Kích thước tối đa của Stack. `MaxStack = _maxStack;` với `_maxStack` là tham số truyền vào constructor
- `Items`: khởi tạo mảng với số lượng phần tử `_maxStack` là tham số truyền vào
- `Top = -1`: Đánh dấu Stack hiện tại là trống.

## Các thao tác cơ bản trên Stack:

### void Push(int item) - Thực hiện thêm một phần tử vào Stack:
- Kiểm tra nếu Stack đã đầy (`Top == MaxStack - 1`):
  - Throw exception `Stack is full`.
- Ngược lại:
  - Tăng giá trị `Top` lên 1.
  - Gán phần tử `Items` vào vị trí `Top` của mảng `Items`.

### int Pop() - Thực hiện loại bỏ phần tử vào cuối Stack và trả về giá trị của phần tử đó:
- Kiểm tra nếu Stack rỗng (tức là Top == -1):
  - Throw exception `Stack is empty`.
- Ngược lại:
  - Tạo biến `temp` và lấy giá trị tại vị trí `Top` trong mảng `Items`.
  - Giảm giá trị `Top` đi 1.
  - Trả về `temp`.

### Peek() - Trả về giá trị của phần tử đầu Stack mà không xóa nó:
- Kiểm tra nếu Stack rỗng (`Top == -1`):
  - Throw exception `Stack is empty`.
- Ngược lại:
  - Trả về giá trị của phần tử tại vị trí `Top` trong mảng `Items`.

### bool IsEmpty - Kiểm tra xem Stack có rỗng không:
- Trả về kết quả `Top > -1` (nghĩa là Stack không rỗng).

### bool IsFull - Kiểm tra xem Stack có đầy không:
- Trả về kết quả `Top == MaxStack - 1` (nghĩa là Stack đã đầy).

### int Count - Đếm số lượng phần tử trong Stack:
- Trả về giá trị `Top + 1` (số lượng phần tử là `Top + 1`).


## code mẫu - Stack (Ngăn xếp)

Dưới đây là một ví dụ về việc triển khai class Stack trong ngôn ngữ C#:

```csharp
public class Stack
{
    public int[] Items;
    public int Top = -1;
    public int MaxStack;

    // Khởi tạo một ngăn xếp với số lượng item
    public Stack(int _maxStack)
    {
        MaxStack = _maxStack;
        Items = new int[MaxStack];
        Top = -1;
    }

    // Thêm một phần tử vào ngăn xếp
    public void Push(int item)
    {
        throw new NotImplementedException();
    }

    // Lấy và xóa phần tử đầu ngăn xếp
    public int Pop()
    {
        throw new NotImplementedException();
    }

    // Trả về giá trị của phần tử đầu ngăn xếp mà không xóa nó
    public int Peek()
    {
        throw new NotImplementedException();
    }

    // Kiểm tra xem ngăn xếp có rỗng hay không
    public bool IsEmpty()
    {
        throw new NotImplementedException();
    }

    // Kiểm tra xem ngăn xếp đã đầy hay không
    public bool IsFull()
    {
        throw new NotImplementedException();
    }

    // Đếm số lượng phần tử trong ngăn xếp
    public int Count()
    {
        throw new NotImplementedException();
    }
}
```

## Trong hàm main khởi tạo stack và thao tác với stack
```csharp
try
{
    DemoStack demo = new DemoStack(3);
    demo.Push(1);
    demo.Push(2);
    demo.Push(3);
    Console.WriteLine("Peek: " + demo.Peek());
    Console.WriteLine("Count: " + demo.Count());
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Count: " + demo.Count());
    demo.Push(4);
    demo.Push(5);
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Count: " + demo.Count());

}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
```

### Test Case và Kịch Bản cho Bài Tập Stack

#### Test Case 1: Khởi Tạo Stack và Kiểm Tra Tính Chất Ban Đầu

Kịch Bản:
1.  Khởi tạo Stack với kích thước là 3.

Kết Quả Đạt Được:
-   `IsEmpty()`: `True`.
-   `IsFull()`: `False`.
-   `Count()`: `0`.

#### Test Case 2: Thêm Phần Tử vào Stack (Push)

Kịch Bản:
1.  Khởi tạo Stack với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Stack.
3.  Thực hiện thêm phần tử 20 vào Stack.

Kết Quả Đạt Được:
-   `IsEmpty()`: `False`.
-   `IsFull()`: `False`.
-   `Count()`: `2`.

#### Test Case 3: Thêm Phần Tử vào Stack (Push) cho tới khi tràn stack

Kịch Bản:
1.  Khởi tạo Stack với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Stack.
3.  Thực hiện thêm phần tử 20 vào Stack.
4.  Thực hiện thêm phần tử 50 vào Stack.
5.  Thực hiện thêm phần tử 100 vào Stack.

Kết Quả Đạt Được:
-   `throw new Exception("Stack is full");` khi thực hiện push phần tử thứ 4

#### Test Case 4: Lấy Phần Tử Khỏi Stack (Pop)

Kịch Bản:
1.  Khởi tạo Stack với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Stack.
3.  Thực hiện thêm phần tử 20 vào Stack.
4.  Thực hiện lấy phần tử từ Stack.
5.  Kiểm tra giá trị phần tử lấy được.

Kết Quả Đạt Được:
-   Giá trị phần tử lấy được: Phù hợp.
-   `IsEmpty()`: `False`.
-   `IsFull()`: `False`.
-   `Count()`: `1`.
-   phần tử lấy được có giá trị là `20` vì 20 vào sau.


#### Test Case 5: Lấy nhiều hơn số Phần Tử trong Stack (Pop)

Kịch Bản:
1.  Khởi tạo Stack với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Stack.
3.  Thực hiện thêm phần tử 20 vào Stack.
4.  Thực hiện thêm phần tử 50 vào Stack.
5.  Thực hiện gọi hàm `Pop() 4 lần`.

Kết Quả Đạt Được:
-   `throw new Exception("Stack is empty");` khi gọi hàm Pop lần thứ 4

#### Test Case 6: Kiểm Tra Peek (Top)

Kịch Bản:

1.  Khởi tạo Stack với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Stack.
3.  Thực hiện thêm phần tử 20 vào Stack.
4.  Thực hiện gọi hàm `Top()`.

Kết Quả Đạt Được:

-   Giá trị trả về là 20
-   `IsEmpty()`: `False`.
-   `IsFull()`: `False`.
-   `Count()`: `2` vì lấy ra chứ không xóa khỏi stack

#### Test Case 7: Kiểm Tra Xóa Hết Phần Tử

Kịch Bản:

1.  Khởi tạo Stack với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Stack.
3.  Thực hiện thêm phần tử 20 vào Stack.
3.  Thực hiện thêm phần tử 50 vào Stack.
1.  Thực hiện lấy phần tử từ Stack cho đến khi Stack rỗng.

Kết Quả Đạt Được:

-   `IsEmpty()`: `True`.
-   `IsFull()`: `False`.
-   `Count()`: `0`.
### Dựa trên các kịch bản trên hãy viết unit test cho class Stack