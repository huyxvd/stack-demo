﻿DemoStack demo = new DemoStack(3);
demo.Push(1);
demo.Push(2);
demo.Push(3);

try
{
    demo.Push(4); // Stack full
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
Console.WriteLine("Peek: " + demo.Peek());
Console.WriteLine("Count: " + demo.Count());
Console.WriteLine("Pop: " + demo.Pop());
Console.WriteLine("Pop: " + demo.Pop());
Console.WriteLine("Pop: " + demo.Pop());
Console.WriteLine("Count: " + demo.Count());
demo.Push(4);
demo.Push(5);
Console.WriteLine("Pop: " + demo.Pop());
Console.WriteLine("Pop: " + demo.Pop());
Console.WriteLine("Count: " + demo.Count()); 

