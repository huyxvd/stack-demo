﻿StackWithMenu stackWithMenu = new StackWithMenu();
stackWithMenu.ShowOffMenu();

class StackWithMenu
{
    public DemoStack stack { get; set; }
    public StackWithMenu()
    {
        stack = new DemoStack(10);
    }
    public void ShowOffMenu()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        string choice = string.Empty;
        while (choice != "5")
        {
            Console.WriteLine("Menu:");
            Console.WriteLine("0. Thay đổi số phần tử ");
            Console.WriteLine("1. Xem số lượng phần tử trong Stack");
            Console.WriteLine("2. Thêm phần tử vào Stack");
            Console.WriteLine("3. Lấy phần tử ra khỏi Stack");
            Console.WriteLine("4. Xem giá trị của phần tử đầu Stack");
            Console.WriteLine("5. Thoát");

            Console.Write("Chọn một chức năng (1-5): ");
            choice = Console.ReadLine();

            switch (choice)
            {
                case "0":
                    Console.WriteLine("0. Thay đổi max stack");
                    int maxStack = Convert.ToInt32(Console.ReadLine());
                    stack = new DemoStack(maxStack); // gán lại max stack
                    break;
                case "1":
                    Console.WriteLine($"Số lượng phần tử trong Stack: {stack.Count()}");
                    break;
                case "2":
                    Console.Write("Nhập giá trị phần tử muốn thêm vào Stack: ");
                    int newItem = Convert.ToInt32(Console.ReadLine());
                    stack.Push(newItem);
                    Console.WriteLine($"Đã thêm phần tử {newItem} vào Stack");
                    break;
                case "3":
                    try
                    {
                        int poppedItem = stack.Pop();
                        Console.WriteLine($"Phần tử được lấy ra khỏi Stack: {poppedItem}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case "4":
                    try
                    {
                        int topItem = stack.Peek();
                        Console.WriteLine($"Giá trị của phần tử đầu Stack: {topItem}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case "5":
                    Console.WriteLine("Chương trình kết thúc.");
                    return;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ.");
                    break;
            }

            Console.WriteLine();
        }
    }
}